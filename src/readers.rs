use std::io::{self, Read, Write, Error, ErrorKind};
use byteorder::{WriteBytesExt, ByteOrder, BigEndian};
use std::fs::{OpenOptions, File};
use ::structs::{Res, ErrRes, BatchRecord};
use nom::CompareResult::Incomplete;
use crossbeam_channel as channel;
use std::net::{TcpListener, TcpStream};
use std::net::UdpSocket;
use rmp_serde as rmps;

pub trait InputReader {
    fn read(&self) -> ();
}

pub struct FileReader {
    file: File,
    batch_channel: channel::Sender<BatchRecord>
}

impl FileReader {
    pub fn new(path: String, chan: channel::Sender<BatchRecord>) -> FileReader {
        info!("Creating File reader ...");
        let msg = format!("Opening file `{}` with permissions `r`", &path);
        let file = open_or_panic(
            OpenOptions::new().read(true).write(false).create(false).append(false).open(path),
            msg
        );

        FileReader {
            file,
            batch_channel: chan,
        }
    }
}

impl InputReader for FileReader {
    fn read(&self) {
        loop {
            let loop_control = _handle_readable(&self.file, &self.batch_channel);
            if let Loop::Break = loop_control {
                break;
            }
        }
    }
}

pub struct TcpReader {
    listener: TcpListener,
    chan: channel::Sender<BatchRecord>,
}

impl TcpReader {
    pub fn new(host: String, port: u16, chan: channel::Sender<BatchRecord>) -> TcpReader {
        info!("Starting TcpReader ...");
        let listener = open_or_panic(
            TcpListener::bind(format!("{}:{}", host, port)),
            format!("Binding TCP listener to {}:{}", host, port)
        );

        TcpReader {
            listener,
            chan
        }
    }
}

impl InputReader for TcpReader {
    fn read(&self) -> () {
        info!("Starting TCPReader ... Listening on [{}]", self.listener.local_addr().map(|addr| addr.to_string()).unwrap_or("UNKNOWN".to_owned()));
        for stream_result in self.listener.incoming() {
            debug!("Incomming TCP connection ...");
            match stream_result {
                Ok(mut stream) => {
                    debug!("Spawning new thread for communication with TCP client [{}]", stream.peer_addr().map(|addr| {addr.to_string()}).unwrap_or("UNKNOWN".to_owned()));
                    let chan = self.chan.clone();
                    ::std::thread::spawn(move || {
                        loop {
                            let loop_control= _handle_readable(&stream, &chan);
                            if let Loop::Break = loop_control {
                                break;
                            }
                        }
                    });
                },
                Err(e) => {
                    error!("TCP connection failed, due to: {}", e)
                }
            }
        }
    }
}

fn _handle_readable<R>(stream: R, chan_clone: &channel::Sender<BatchRecord>)
    -> Loop
    where R: Read + Sized
{
    match rmps::decode::from_read::<R, BatchRecord>(stream) {
        Ok(batch_record) => {
            chan_clone.send(batch_record);
            Loop::Continue
        },
        Err(rmps::decode::Error::InvalidMarkerRead(ref e)) if e.kind() == ErrorKind::UnexpectedEof =>{
            debug!("EOF reached!");
            Loop::Break
        },
        Err(e) => {
            error!("Reader panicked due to {:?}", e);
            panic!("Reader panicked due to {:?}", e)
        }
    }
}

enum Loop {
    Continue,
    Break
}

pub struct UdpSocketReader {
    socket: UdpSocket,
}

impl Read for UdpSocketReader {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        self.socket.recv(buf)
    }
}

impl <'a> Read for &'a UdpSocketReader {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        self.socket.recv(buf)
    }
}

pub struct UdpReader {
    socket_reader: UdpSocketReader,
    chan: channel::Sender<BatchRecord>
}

impl UdpReader {
    pub fn new(host: String, port: u16, chan: channel::Sender<BatchRecord>) -> UdpReader {
        info!("Starting UdpReader ...");
        let socket = open_or_panic(
            UdpSocket::bind(format!("{}:{}", host, port)),
            format!("Binding to UDP socket {}:{}", host, port)
        );

        UdpReader {
            socket_reader: UdpSocketReader {
                socket
            },
            chan
        }
    }
}

impl InputReader for UdpReader {
    fn read(&self) -> () {
        _handle_readable(&self.socket_reader, &self.chan);
    }
}

pub struct StdInReader {
    stdin: io::Stdin,
    chan: channel::Sender<BatchRecord>
}

impl StdInReader {
    pub fn new(chan: channel::Sender<BatchRecord>) -> StdInReader {
        info!("Starting StdInReader ...");
        let stdin = io::stdin();

        StdInReader {
            stdin,
            chan
        }
    }
}

impl InputReader for StdInReader {
    fn read(&self) -> () {
        {
            debug!("Acquiring stdin lock");
            let stdin_handle = &mut self.stdin.lock();
            _handle_readable(stdin_handle, &self.chan);
        }
        debug!("Releasing stdin lock");
    }
}

fn open_or_panic<T>(res: Result<T, ::std::io::Error>, msg: String) -> T {
    match res {
        Ok(v) => {
            info!("{}\t{}", msg, "[OK]");
            v
        },
        Err(e) => {
            error!("{}\t{}", msg, "[FAILED]");
            panic!("{}\t{}", msg, "[FAILED]");
        }
    }
}


pub fn create_input_reader(input: &::Input, chan: channel::Sender<BatchRecord>) -> Box<InputReader> {
    match input {
        ::Input::StdIn => Box::new(StdInReader::new(chan)),
        ::Input::Tcp {host, port} => Box::new(TcpReader::new(host.clone(), port.clone(), chan)),
        ::Input::Udp {host, port} => Box::new(UdpReader::new(host.clone(), port.clone(), chan)),
        ::Input::File {path} => Box::new(FileReader::new(path.clone(), chan)),
        inp_type => {
            error!("Unsupported input type selected: {:?}", inp_type);
            panic!("Unsupported input type selected: {:?}", inp_type);
        }
    }
}
