use ::md5::Digest;
use std::io::Write;
use byteorder::{WriteBytesExt, ByteOrder, BigEndian};
use std::net::{Ipv4Addr, Ipv6Addr, IpAddr};
use serde::ser::{Serialize, Serializer};
use serde::de::Deserialize;
pub const BATCH_TAG: [u8; 2] = [0x11, 0x01];
pub const BATCH_END_TAG: [u8; 2] = [0x01, 0x11];
pub const HEADER_TAG: [u8; 2] = [0x00, 0x11];
pub const HEADER_END_TAG: [u8; 2] = [0x11, 0x00];
pub const HEADER_OFFSET: usize = 12;

#[derive(Debug, PartialEq, Deserialize)]
pub struct BatchRecord {
    chksum: [u8; 16],
    headers: Vec<HeaderRecord>
}

impl BatchRecord {
    pub fn headers(&self) -> &Vec<HeaderRecord> {
        &self.headers
    }

    pub fn chksum(&self) -> Digest {
        Digest(self.chksum.clone())
    }
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct HeaderRecord {
    timestamp: u64,
    payload: Vec<u8>
}

impl HeaderRecord {
    pub fn timestamp(&self) -> u64 {
        self.timestamp
    }

    pub fn payload(&self) -> &[u8] {
        &self.payload[..]
    }
}

pub type Res<T> = Result<T, String>;
pub type ErrRes = Res<()>;

pub struct Buffer {
    buf: [u8; 10000],
    from: usize,
    read: usize,
}

impl Buffer {
    pub fn new() -> Buffer {
        Buffer {
            buf: [0; 10000],
            from: 0,
            read: 0,
        }
    }

    pub fn as_slice_mut(&mut self) -> &mut [u8] {
        &mut self.buf[self.from..]
    }

    pub fn set_read(&mut self, read: usize) -> ErrRes {
        if read + self.from > self.buf.len() {
            return Err(format!("Read size: `{}` can not be greater than buffer size `{}`", self.from + read, self.buf.len()));
        }
        self.read = read;
        Ok(())
    }

    pub fn as_slice(&self) -> &[u8] {
        &self.buf[..self.from + self.read]
    }

    pub fn push(&mut self, rest: &[u8]) -> ErrRes {
        if rest.len() > self.buf.len() {
            return Err(format!("Slice is too big: {}, but buffer size is: {}", rest.len(), self.buf.len()));
        }
        &mut self.buf[..rest.len()].clone_from_slice(rest);
        Ok(())
    }

}

pub mod ether_types {
    pub const IPV6: u16 = 0x86DD;
    pub const IPV4: u16 = 0x0800;
    pub const ARP: u16 = 0x0806;
    pub const VLAN: u16 = 0x8100;
}

#[derive(PartialEq, Deserialize)]
pub struct MacAddress([u8; 6]);

impl MacAddress {
    pub fn new(mac_address: [u8; 6]) -> MacAddress {
        MacAddress(mac_address)
    }
}

impl Serialize for MacAddress {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        let mac_address = format!("{:X}:{:X}:{:X}:{:X}:{:X}:{:X}", self.0[0], self.0[1], self.0[2], self.0[3], self.0[4], self.0[5]);
        serializer.serialize_str(mac_address.as_str())
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct EthernetFrameHeader {
    source: MacAddress,
    destination: MacAddress,
    ether_type: u16,
    vlan: Option<u16>
}

impl EthernetFrameHeader {
    pub fn new(source: MacAddress, destination: MacAddress, ether_type: u16, vlan: Option<u16>)
        -> EthernetFrameHeader
    {
        EthernetFrameHeader {
            source,
            destination,
            ether_type,
            vlan,
        }
    }

    pub fn get_ether_type(&self) -> u16 {
        self.ether_type
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct IPv4Header {
    version: u8,
    ihl: u8,
    ecn: u8,
    total_length: u16,
    id: u16,
    flags: u8,
    fragment_offset: u16,
    ttl: u8,
    protocol: u8,
    check_sum: u16,
    source: Ipv4Addr,
    destination: Ipv4Addr,
    options: Vec<u8>
}

impl IPv4Header {
    pub fn new(version: u8, ihl: u8, ecn: u8,
               total_length: u16, id: u16, flags: u8,
               fragment_offset: u16, ttl: u8, protocol: u8,
               check_sum: u16, source: Ipv4Addr, destination: Ipv4Addr,
               options: Vec<u8>)
        -> IPv4Header
    {
        IPv4Header {
            version,
            ihl,
            ecn,
            total_length,
            id,
            flags,
            fragment_offset,
            ttl,
            protocol,
            check_sum,
            source,
            destination,
            options
        }
    }

    pub fn get_protocol(&self) -> u8 {
        self.protocol
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct IPv6Header {
    version: u8,
    traffic_class: u8,
    flow_label: u32,
    payload_length: u16,
    next_header: u8,
    hop_limit: u8,
    source: Ipv6Addr,
    destination: Ipv6Addr,
    extension_headers: Vec<IPv6ExtHeader>,
    protocol: u8
}

impl IPv6Header {
    pub fn new(version: u8, traffic_class: u8, flow_label: u32,
               payload_length: u16, next_header: u8, hop_limit: u8,
               source: Ipv6Addr, destination: Ipv6Addr)
        -> IPv6Header
    {
        IPv6Header {
            version,
            traffic_class,
            flow_label,
            payload_length,
            next_header,
            hop_limit,
            source,
            destination,
            extension_headers: Vec::new(),
            protocol: 0
        }
    }

    pub fn get_next_header(&self) -> u8 {
        self.next_header
    }

    pub fn get_protocol(&self) -> u8 {
        self.protocol
    }

    pub fn set_protocol(&mut self, protocol: u8) {
        self.protocol = protocol;
    }

    pub fn push_ext_header(&mut self, ext_header: IPv6ExtHeader) {
        self.extension_headers.push(ext_header);
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct IPv6ExtHeader {
    next_hdr: Ipv6NextHeader,
    data1: [u8; 2],
    data2: [u8; 4],
    ext_data: Vec<u8>,
    current_hdr_type: u8
}

impl IPv6ExtHeader {
    pub fn new(next_hdr: Ipv6NextHeader, data1: [u8; 2], data2: [u8; 4], ext_data: Vec<u8>)
        -> IPv6ExtHeader
    {
        IPv6ExtHeader {
            next_hdr,
            data1,
            data2,
            ext_data,
            current_hdr_type: 0
        }
    }

    pub fn get_next_hdr(&self) -> &Ipv6NextHeader {
        &self.next_hdr
    }

    pub fn get_next_hdr_mut(&mut self) -> &mut Ipv6NextHeader {
        &mut self.next_hdr
    }

    pub fn get_current_hdr_type(&self) -> u8 {
        self.current_hdr_type
    }

    pub fn set_current_hdr_type(&mut self, header_type: u8) {
        self.current_hdr_type = header_type;
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct Ipv6NextHeader {
    next_header: u8,
    header_ext_length: u8
}

impl Ipv6NextHeader {
    pub fn new(next_header: u8, header_ext_length: u8) -> Ipv6NextHeader {
        Ipv6NextHeader {
            next_header,
            header_ext_length,
        }
    }

    pub fn get_next_header(&self) -> u8 {
        self.next_header
    }

    pub fn set_next_header(&mut self, next_header: u8) {
        self.next_header = next_header;
    }

    pub fn get_ext_header_length(&self) -> u8 {
        self.header_ext_length
    }
}

pub mod ipv6_next_headers {
    pub const HOP_BY_HOP_OPTIONS: u8 = 0;
    pub const DESTINATION_OPTIONS: u8 = 60;
    pub const ROUTING: u8 = 43;
    pub const FRAGMENT: u8 = 44;
    pub const AUTHENTICATION_HEADER: u8 = 51;
    pub const ESP: u8 = 50;
    pub const MOBILITY: u8 = 135;
    pub const HOST_IDENTITY_PROTOCOL: u8 = 139;
    pub const SHIM_6_PROTOCOL: u8 = 140;
    pub const RESERVED_1: u8 = 253;
    pub const RESERVED_2: u8 = 254;

    pub const HEADERS: [u8; 11] = [
        HOP_BY_HOP_OPTIONS,
        DESTINATION_OPTIONS,
        ROUTING,
        FRAGMENT,
        AUTHENTICATION_HEADER,
        ESP,
        MOBILITY,
        HOST_IDENTITY_PROTOCOL,
        SHIM_6_PROTOCOL,
        RESERVED_1,
        RESERVED_2
    ];

    pub fn is_ipv6_ext_header(next_header: u8) -> bool {
        HEADERS.iter().any(|hdr_type| next_header == *hdr_type)
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct ArpHeader {
    hardware_type: u16,
    protocol_type: u16,
    hw_address_len: u8,
    proto_address_len: u8,
    operation: u16,
    sender_hw_address: MacAddress,
    sender_proto_address: [u16; 2],
    target_hw_address: MacAddress,
    target_proto_address: [u16; 2]
}

impl ArpHeader {
    pub fn new(hardware_type: u16, protocol_type: u16, hw_address_len: u8,
               proto_address_len: u8, operation: u16, sender_hw_address: MacAddress,
               sender_proto_address: [u16; 2], target_hw_address: MacAddress,
               target_proto_address: [u16;2])
        -> ArpHeader
    {
        ArpHeader {
            hardware_type,
            protocol_type,
            hw_address_len,
            proto_address_len,
            operation,
            sender_hw_address,
            sender_proto_address,
            target_hw_address,
            target_proto_address,
        }
    }
}

pub mod segment_types {
    pub const TCP: u8 = 6;
    pub const UDP: u8 = 17;
    pub const ICMP: u8 = 1;
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct TcpHeader {
    source: u16,
    destination: u16,
    sequence_number: u32,
    ack_number: u32,
    data_offset: u8,
    flags: u16,
    window_size: u16,
    check_sum: u16,
    urgent_pointer: u16
}

impl TcpHeader {
    pub fn new(source: u16, destination: u16, sequence_number: u32,
               ack_number: u32, data_offset: u8, flags: u16, window_size: u16,
               check_sum: u16, urgent_pointer: u16)
        -> TcpHeader
    {
        TcpHeader {
            source,
            destination,
            sequence_number,
            ack_number,
            data_offset,
            flags,
            window_size,
            check_sum,
            urgent_pointer,
        }
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct UdpHeader {
    source: u16,
    destination: u16,
    length: u16,
    check_sum: u16
}

impl UdpHeader {
    pub fn new(source: u16, destination: u16, length: u16, check_sum: u16) -> UdpHeader {
        UdpHeader {
            source,
            destination,
            length,
            check_sum,
        }
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub struct IcmpHeader {
    type_: u8,
    code: u8,
    check_sum: u16
}

impl IcmpHeader {
    pub fn new(type_: u8, code: u8, check_sum: u16) -> IcmpHeader {
        IcmpHeader {
            type_,
            code,
            check_sum,
        }
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub enum L3Packet {
    Ipv4(IPv4Header),
    Ipv6(IPv6Header),
    Arp(ArpHeader)
}
#[derive(Serialize, Deserialize)]
pub struct ParsedPacket {
    frame: EthernetFrameHeader,
    l3_packet: L3Packet,
    l4_segment: Option<L4Segment>,
    timestamp: u64,
}

impl ParsedPacket {
    pub fn new(frame: EthernetFrameHeader, l3_packet: L3Packet,
               l4_segment: Option<L4Segment>, timestamp: u64)
        -> ParsedPacket
    {
        ParsedPacket {
            frame,
            l3_packet,
            l4_segment,
            timestamp,
        }
    }

    pub fn new_l3(frame: EthernetFrameHeader, l3_packet: L3Packet, timestamp: u64)
        -> ParsedPacket
    {
        ParsedPacket {
            frame,
            l3_packet,
            l4_segment: None,
            timestamp,
        }
    }

    pub fn new_l4(frame: EthernetFrameHeader, l3_packet: L3Packet,
               l4_segment: L4Segment, timestamp: u64)
               -> ParsedPacket
    {
        ParsedPacket {
            frame,
            l3_packet,
            l4_segment: Some(l4_segment),
            timestamp,
        }
    }

    pub fn frame(&self) -> &EthernetFrameHeader {
        &self.frame
    }

    pub fn l3packet(&self) -> &L3Packet {
        &self.l3_packet
    }

    pub fn l4segment(&self) -> &Option<L4Segment> {
        &self.l4_segment
    }

    pub fn timestamp(&self) -> u64 {
        self.timestamp
    }
}

#[derive(PartialEq, Serialize, Deserialize)]
pub enum L4Segment {
    Tcp(TcpHeader),
    Udp(UdpHeader),
    Icmp(IcmpHeader)
}
