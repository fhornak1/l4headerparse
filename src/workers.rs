
use ::input_parse::parse;
use ::crossbeam_channel as channel;

use ::structs::{BatchRecord, HeaderRecord, Res, ErrRes, ParsedPacket};
use ::output_write::{serialize_json, serialize_mpac, Output};


fn process_batch(batch: BatchRecord) -> Res<Vec<ParsedPacket>> {
    let mut result: Vec<ParsedPacket> = Vec::new();
    for header in batch.headers() {
        match parse(header.timestamp(), header.payload()) {
            Ok(pkt) => {
                result.push(pkt);
            },
            Err(err) => {
                error!("Error occured while parsing packet header, {:?}", err);
            }
        }
    }

    Ok(result)
}

pub struct WorkerPool {
    handles: Vec<::std::thread::JoinHandle<()>>,
    rx: channel::Receiver<BatchRecord>,
    tx: channel::Sender<Output>,
    size: usize,
    formatter: fn(ParsedPacket) -> Output
}

impl WorkerPool {
    pub fn new(workers: usize, rx: channel::Receiver<BatchRecord>, tx: channel::Sender<Output>, format: &::OutputFormat)
        -> WorkerPool
    {
        let formatter = match format {
            ::OutputFormat::JSON => serialize_json,
            ::OutputFormat::MsgPack => serialize_mpac
        };
        WorkerPool {
            handles: Vec::with_capacity(workers),
            rx,
            tx,
            size: workers,
            formatter
        }
    }

    pub fn start(&mut self) -> () {
        for k in 0 .. self.size {
            let rx = self.rx.clone();
            let tx = self.tx.clone();
            let fmt = self.formatter;
            self.handles.push(::std::thread::Builder::new().name(format!("Worker-{}", k)).spawn(move || {
                for batch in rx {
                    match process_batch(batch) {
                        Ok(valid_batch) => {
                            for packet in valid_batch {
                                tx.send((fmt)(packet))
                            }
                        },
                        Err(e) => {
                            warn!("Processing of batch failed, due to {}", e);
                            warn!("Batch data: {:?}", e);
                        }
                    }
                }
            }).unwrap());
        }
    }

    pub fn join(self) -> () {
        for hnd in self.handles {
            hnd.join().unwrap();
        }
    }
}
