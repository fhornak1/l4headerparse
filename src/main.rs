#[macro_use]
extern crate nom;
#[macro_use]
extern crate arrayref;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;

extern crate stderrlog;

extern crate serde;
extern crate serde_json;
extern crate rmp_serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate clap;

extern crate byteorder;

extern crate crossbeam;
extern crate crossbeam_channel;

extern crate md5;

#[macro_use]
extern crate elastic_types_derive;
extern crate elastic;
extern crate elastic_types;

mod input_parse;
mod output_write;
mod workers;
mod readers;
mod structs;

use output_write::{create_output_writer, OutputWriter};
use readers::{create_input_reader, InputReader};
use workers::WorkerPool;
use std::str::FromStr;

use crossbeam_channel as channel;


#[derive(Debug, Clone)]
pub struct Config {
    input: Input,
    output: OutputCfg,
    format: OutputFormat,
    workers: usize,
    timestamp: stderrlog::Timestamp,
    verbosity: usize,
    quiet: bool
}

impl Config {
    fn from_args() -> Config {
        let matches = clap_app!(l4headparse =>
            (version: "1.0")
            (author: "Filip Hornak <fhornak1@gmail.com")
            (about: "Application parsing captured packet headers, and sending them to specified output.")
            (@arg INPUT: -i --input +takes_value default_value[stdin] "Sets input which will be read and processed. Options {'stdin', 'tcp://<host:port>', 'udp://<host:port>', 'capture://<interface>', 'file://<path>'}")
            (@arg OUTPUT: -o --output +takes_value default_value[stdout] "Sets output where parsed results will be written. Options {'stdout', 'tcp://<host:port>', 'udp://<host:port>'}")
            (@arg FORMAT: -f --format +takes_value default_value[json] possible_value[json msgpack] "Sets output stream format")
            (@arg WORKERS: -w --workers +takes_value "Sets how many parser workers will parse inputs. Defaults to 1.")
            (@arg BATCHES: -b --batchsize +takes_value "Sets output batch size. Defaults to 1")
            (@arg VERBOSITY: -v ... "Sets the verbosity level")
            (@arg QUIET: -q --quiet "Silence all logging output")
            (@arg TIMESTAMP: -t --timestamp +takes_value default_value[us] possible_value[none sec ms us ns] "Sets timestamp precision for logs, if any")

        ).get_matches();
        let workers: usize = matches.value_of("WORKERS").unwrap_or("1").parse().unwrap();
        let input: Input = Input::from_args(matches.value_of("INPUT").unwrap_or("stdin")).unwrap();
        let output_fmt: OutputFormat = OutputFormat::from_args(matches.value_of("FORMAT").unwrap_or("json")).unwrap();
        let output: OutputCfg = OutputCfg::from_arg(matches.value_of("OUTPUT").unwrap_or("stdout")).unwrap();
        let timestamp = matches.value_of("TIMESTAMP").map(|v| {
            stderrlog::Timestamp::from_str(v).unwrap_or_else(|_| {
                clap::Error {
                    message: "invalid value for 'TIMESTAMP'".into(),
                    kind: clap::ErrorKind::InvalidValue,
                    info: None
                }.exit()
            })
        }).unwrap_or(stderrlog::Timestamp::Microsecond);

        Config {
            input,
            output,
            format: output_fmt,
            workers,
            timestamp,
            verbosity: matches.occurrences_of("VERBOSITY") as usize,
            quiet: matches.is_present("QUIET")
        }
    }

    fn format(&self) -> String {
        format!("{:?}", self)
    }
}

#[derive(Debug, Clone)]
pub enum Input {
    StdIn,
    Tcp {host: String, port: u16},
    Udp {host: String, port: u16},
//    Capture {interface: String},
    File {path: String}
}

impl Input {
    fn from_args(arg: &str) -> Result<Input, String> {
        let kind_args = arg.splitn(2, "://").collect::<Vec<&str>>();

        match kind_args[0] {
            "stdin" => Ok(Input::StdIn),
            "file" => Ok(Input::File {path: kind_args[1].to_owned()}),
            "tcp" => {
                let (host, port) = _parse_host_port_pair(kind_args[1])?;
                Ok(Input::Tcp {host, port})
            },
            "udp" => {
                let (host, port) = _parse_host_port_pair(kind_args[1])?;
                Ok(Input::Udp {host, port})
            },
            _ => Err(format!("Unknown Input type: {}", kind_args[0]))
        }
    }
}

fn _parse_host_port_pair(address: &str)
    -> Result<(String, u16), String>
{
    let host_port_pair = address.splitn(2, ":").collect::<Vec<&str>>();

    if host_port_pair.len() < 2 {
        Err("Invalid address format, should be in format: <ADDRESS>:<PORT>".to_owned())
    } else {
        let port: u16 = match host_port_pair[1].parse() {
            Ok(p) => Ok(p),
            Err(e) => Err(format!("Unable to parse port number from `{}`, number should be of type u16 [0 .. 65535]", host_port_pair[1]))
        }?;

        Ok((host_port_pair[0].to_owned(), port))
    }
}

#[derive(Debug,Clone)]
pub enum OutputCfg {
    StdOut,  // stdout
    Tcp {host: String, port: u16},  // tcp://10.10.10.20:9876
    Udp {host: String, port: u16},  // udp://10.20.20.1:15679
    File {path: String},  // file:///dev/pflog.1
    Kafka {  // kafka://{kafka-1.host.com:9092,kafka-2.host.com:9093}[/1][/One]
        from_hosts: Vec<String>,
        ack_timeout: u64,
        required_acks: String,
        topic: String
    },
    RabbitMQ {  // rabbit://user:password@host:port[/use_ssl=1][/verify_ssl=1]
        host: String,
        port: u16,
        user: String,
        password: String,
        use_ssl: bool,
        verify_ssl: bool
    },
    ElasticSearch {  // elasticsearch://host:port/_index/type
        url: String,
        index: String,
        bulk: Option<usize>
    }
}

impl OutputCfg {
    fn from_arg(arg: &str) -> Result<OutputCfg, String> {
        let kind_argv = arg.splitn(2, "://").collect::<Vec<&str>>();

        match kind_argv[0] {
            "stdout" => Ok(OutputCfg::StdOut),
            "file" => Ok(OutputCfg::File {path: kind_argv[1].to_owned()}),
            "tcp" => {
                let (host, port) = _parse_host_port_pair(kind_argv[1])?;

                Ok(OutputCfg::Tcp {host, port})
            },
            _ => Err(format!("Unknown Output type `{}`.", kind_argv[0]))
        }
    }
}

#[derive(Debug,Clone)]
pub enum OutputFormat {
    JSON,
    MsgPack
}

impl OutputFormat {
    fn from_args(argv: &str) -> Result<OutputFormat, String> {
        match argv {
            "json" => Ok(OutputFormat::JSON),
            "msgpack" => Ok(OutputFormat::MsgPack),
            _ => Err(format!("Unknonw Output Format option `{}`", argv))
        }
    }
}

lazy_static! {
    pub static ref CONFIG: Config = Config::from_args();
}

fn main() {
    stderrlog::new()
        .module(module_path!())
        .timestamp(CONFIG.timestamp)
        .quiet(CONFIG.quiet)
        .verbosity(CONFIG.verbosity)
        .init()
        .unwrap();

    info!("Starting {}-{}, with:\n{}", crate_name!(), crate_version!(), CONFIG.format());

    let (batch_tx, batch_rx) = channel::unbounded::<structs::BatchRecord>();
    let (output_tx, output_rx) = channel::unbounded::<output_write::Output>();

    let mut pool = WorkerPool::new(CONFIG.workers, batch_rx, output_tx, &CONFIG.format);

    ::std::thread::Builder::new().name("Reader".to_owned()).spawn(move || {
        let mut reader = create_input_reader(&CONFIG.input, batch_tx);
        reader.read();
    }).unwrap();

    ::std::thread::Builder::new().name("Writer".to_owned()).spawn(move || {
        let mut writer = create_output_writer(&CONFIG.output).unwrap();
        loop {
            match output_rx.recv() {
                Some(data) => {
                    writer.write_output(data).unwrap();
                },
                None => {}
            }
        }
    }).unwrap();

    pool.start();
    pool.join();

//    use rmp_serde as rmps;
//    use std::fs::{OpenOptions, File};
//
//    let read_file = OpenOptions::new().read(true).open("/home/filip/capture_msgpack.pcap").unwrap();
//    loop {
//        match rmps::decode::from_read::<&File, ::structs::BatchRecord>(&read_file) {
//            Ok(d) => println!("{:?}", d),
//            Err(rmp_serde::decode::Error::InvalidMarkerRead(ref e)) if e.kind() == std::io::ErrorKind::UnexpectedEof => break,
//            Err(e) => panic!("{:?}", e)
//        };
//    }

}
