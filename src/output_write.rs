
use serde::{Serialize, Deserialize};
use serde_json;
use rmp_serde::{Serializer, Deserializer};
use structs::ParsedPacket;
use std::io::Write;
use std::net::{UdpSocket, TcpStream};

pub fn serialize_mpac(packet: ParsedPacket) -> Output {
    let mut buffer = Vec::new();

    packet.serialize( &mut Serializer::new(&mut buffer)).unwrap();

    Output::MsgPack(buffer)
}

pub fn serialize_json(packet: ParsedPacket) -> Output {
    let output = serde_json::to_string(&packet).unwrap();

    Output::Json(output)
}

pub enum Output {
    Json(String),
    MsgPack(Vec<u8>),
}

pub trait OutputWriter {
    fn write_output(&mut self, output: Output) -> Result<(), String>;
}

pub struct FileWriter {
    file: ::std::fs::File,
}

impl FileWriter {
    pub fn new(path: String) -> FileWriter {
        let file = ::std::fs::OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .open(path)
            .unwrap();
        FileWriter {
            file
        }
    }
}

impl Write for FileWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        self.file.write(buf)
    }

    fn flush(&mut self) -> Result<(), std::io::Error> {
        self.file.flush()
    }
}

impl OutputWriter for FileWriter {
    fn write_output(&mut self, output: Output) -> Result<(), String> {
        match output {
            Output::MsgPack(octets) => {
                match self.file.write_all(&octets[..]) {
                    Err(e) => Err(format!("Can not write output to file, due to: {}", e)),
                    _ => Ok(())
                }
            },
            Output::Json(json_data) => {
                match self.file.write_all(json_data.as_bytes()) {
                    Ok(()) => match self.file.write(",".as_bytes()) {
                        Ok(size) => Ok(()),
                        Err(e) => Err(format!("Can not write output to file, due to: {}", e))
                    },
                    Err(e) => Err(format!("Can not write output to file, due to: {}", e))
                }
            }
        }?;
        match self.file.flush() {
            Err(e) => Err(format!("Can not flush data to file, due to: {}", e)),
            _ => Ok(())
        }
    }
}

pub struct BatchWriter<W: Write + Sized> {
    buffer: Vec<u8>,
    capacity: usize,
    writer: W,
}

impl <W: Write + Sized> BatchWriter<W> {
    pub fn new(capacity: usize, writer: W) -> BatchWriter<W> {
        BatchWriter {
            buffer: Vec::with_capacity(capacity),
            capacity,
            writer,
        }
    }
}

impl <W: Write + Sized> OutputWriter for BatchWriter<W> {
    fn write_output(&mut self, output: Output) -> Result<(), String> {
        let octets = _output_to_bytes(&output);

        if octets.len() + self.buffer.len() > self.capacity {
            match self.writer.write_all(&self.buffer[..]) {
                Err(e) => Err(format!("Can not write buffer to writer, due to: {}", e)),
                _ => Ok(())
            }?;
            self.writer.flush().unwrap();
            self.buffer.clear();
        }

        match (&mut self.buffer[..]).write(octets) {
            Err(e) => Err(format!("Can not write to BatchWriter buffer, due to: {}", e)),
            _ => Ok(())
        }?;

        Ok(())
    }
}

pub struct TcpWriter {
    stream: ::std::net::TcpStream,
}

impl TcpWriter {
    pub fn new(host: String, port: u16) -> TcpWriter {
        let stream = TcpStream::connect(format!("{}:{}", host, port)).unwrap();

        TcpWriter {
            stream
        }
    }
}

impl OutputWriter for TcpWriter {
    fn write_output(&mut self, output: Output) -> Result<(), String> {
        let payload = _output_to_bytes(&output);

        match self.write_all(payload) {
            Err(e) => Err(format!("Can not write to TCP stream, due to: {}", e)),
            _ => Ok(())
        }?;

        Ok(())
    }
}

impl Write for TcpWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        self.stream.write(buf)
    }

    fn flush(&mut self) -> Result<(), std::io::Error> {
        self.stream.flush()
    }
}

pub struct UdpWriter {
    socket: ::std::net::UdpSocket
}

impl UdpWriter {
    pub fn new(remote_host: String, remote_port: u16, bind_address: Option<String>) -> UdpWriter {
        let socket = UdpSocket::bind(bind_address.unwrap_or("0.0.0.0:63211".to_owned()))
            .unwrap();
        socket.connect(format!("{}:{}", remote_host, remote_port)).unwrap();

        UdpWriter {
            socket
        }
    }
}

impl Write for UdpWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        self.socket.send(buf)
    }

    fn flush(&mut self) -> Result<(), std::io::Error> {
        Ok(())
    }
}

impl OutputWriter for UdpWriter {
    fn write_output(&mut self, output: Output) -> Result<(), String> {
        let octets = _output_to_bytes(&output);
        match self.write(octets) {
            Err(e) => Err(format!("Can not send data via UDP socket, due to: {}", e)),
            _ => Ok(())
        }?;

        Ok(())
    }
}

pub struct StdOutWriter {
    stdout: ::std::io::Stdout,
}

impl StdOutWriter {
    pub fn new() -> StdOutWriter {
        StdOutWriter {
            stdout: ::std::io::stdout()
        }
    }
}

impl Write for StdOutWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        let mut std_lock = self.stdout.lock();
        std_lock.write(buf)
    }

    fn flush(&mut self) -> Result<(), std::io::Error> {
        let mut std_lock = self.stdout.lock();
        std_lock.flush()
    }
}

impl OutputWriter for StdOutWriter {
    fn write_output(&mut self, output: Output) -> Result<(), String> {
        match output {
            Output::Json(json_data) => {
                match self.write(json_data.as_bytes()) {
                    Ok(s) => {
                        self.write("\n".as_bytes()).unwrap();
                        Ok(())
                    },
                    Err(e) => Err(format!("Can not write to stdout, due to: {}", e))
                }
            },
            Output::MsgPack(_bytes) => {
                Err("Unsupported operation write msgpack to stdout".to_owned())
            }
        }
    }
}

pub struct ElasticWriter {

}

pub struct HttpWriter {

}

pub struct KafkaWriter {

}

pub struct RabbitMQWriter {

}

pub struct RedisWriter {

}

fn _output_to_bytes(output: &Output) -> &[u8] {
    match output {
        Output::Json(string) => string.as_bytes(),
        Output::MsgPack(bytes) => &bytes[..]
    }
}

pub fn create_output_writer(output_cfg: &::OutputCfg)
    -> Result<Box<OutputWriter>, String>
{
    match output_cfg {
        ::OutputCfg::StdOut => Ok(Box::new(StdOutWriter::new())),
        ::OutputCfg::Tcp {host, port} => Ok(Box::new(TcpWriter::new(host.clone(), port.clone()))),
        ::OutputCfg::File {path} => Ok(Box::new(FileWriter::new(path.clone()))),

        _ => Err("Unsupported output config type selected".to_owned())
    }
}