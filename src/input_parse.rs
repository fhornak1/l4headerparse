use nom::Endianness;
use nom::be_u8;
use std::net::{Ipv4Addr, Ipv6Addr, IpAddr};
use structs::{IPv4Header, IPv6ExtHeader, IPv6Header, EthernetFrameHeader, ArpHeader, Ipv6NextHeader, MacAddress};
use structs::{TcpHeader, UdpHeader, IcmpHeader};
use structs::{L3Packet, L4Segment, ParsedPacket, Res};
use structs::{segment_types, ether_types, ipv6_next_headers};

named!(parse_mac_address<&[u8], MacAddress>, map!(take!(6), _create_mac));

named!(ether_frame_wo_vlan<&[u8], EthernetFrameHeader>, do_parse!(
    dest_mac: parse_mac_address >>
    src_mac: parse_mac_address >>
    ether_type: u16!(Endianness::Big) >>
    (
        EthernetFrameHeader::new(
            src_mac,
            dest_mac,
            ether_type,
            Option::None
        )
    )
));

named!(ether_frame_w_vlan<&[u8], EthernetFrameHeader>, do_parse!(
    dest_mac: parse_mac_address >>
    src_mac: parse_mac_address >>
    tag!(&[0x81, 0x00]) >>
    vlan: u16!(Endianness::Big) >>
    ether_type: u16!(Endianness::Big) >>
    (
        EthernetFrameHeader::new(
            src_mac,
            dest_mac,
            ether_type,
            Option::Some(vlan)
        )
    )
));

named!(parse_ether_frame<&[u8], EthernetFrameHeader>, alt!(ether_frame_w_vlan | ether_frame_wo_vlan));

named!(two_nibbles<&[u8], (u8, u8)>, bits!(pair!(take_bits!(u8, 4), take_bits!(u8, 4))));
named!(flag_frag_offset<&[u8], (u8, u16)>, bits!(pair!(take_bits!(u8, 3), take_bits!(u16, 13))));
named!(address<&[u8], Ipv4Addr>, map!(take!(4), _create_ipv4));

named!(parse_ipv4<&[u8], IPv4Header>, do_parse!(
    ver_ihl: two_nibbles >>
    ecn: be_u8 >>
    length: u16!(Endianness::Big) >>
    id: u16!(Endianness::Big) >>
    flags_frag_offset: flag_frag_offset >>
    ttl: be_u8 >>
    protocol: be_u8 >>
    chksum: u16!(Endianness::Big) >>
    src_addr: address >>
    dst_addr: address >>
    options: map!(take!((ver_ihl.1 - 5) * 4), _create_u8_vec) >>
    (
        IPv4Header::new(
            ver_ihl.0,
            ver_ihl.1,
            ecn,
            length,
            id,
            flags_frag_offset.0,
            flags_frag_offset.1,
            ttl,
            protocol,
            chksum,
            src_addr,
            dst_addr,
            options
        )
    )
));

named!(ver_tc_fl<&[u8], (u8, u8, u32)>, bits!(tuple!(take_bits!(u8, 4), take_bits!(u8, 8), take_bits!(u32, 20))));
named!(parse_ipv6<&[u8], Ipv6Addr>, map!(take!(16), _create_ipv6));


named!(parse_ipv6_header<&[u8], IPv6Header>, do_parse!(
    vtcfl: ver_tc_fl >>
    payload_length: u16!(Endianness::Big) >>
    next_hdr: be_u8 >>
    hop_limit: be_u8 >>
    src_addr: parse_ipv6 >>
    dst_addr: parse_ipv6 >>
    (
        IPv6Header::new(
            vtcfl.0,
            vtcfl.1,
            vtcfl.2,
            payload_length,
            next_hdr,
            hop_limit,
            src_addr,
            dst_addr,
        )
    )
));

named!(parse_ipv6_nxt_hdr<&[u8], Ipv6NextHeader>, do_parse!(
    nxt_hdr: be_u8 >>
    hdr_ext_len: be_u8 >>
    (
        Ipv6NextHeader::new(
            nxt_hdr,
            hdr_ext_len
        )
    )
));

named!(parse_ipv6_ext_hdr<&[u8], IPv6ExtHeader>, do_parse!(
    nxt_hdr: parse_ipv6_nxt_hdr >>
    d1: map!(take!(2), _create_2_oct_array) >>
    d2: map!(take!(4), _create_4_oct_array) >>
    opt_data: map!(take!(nxt_hdr.get_ext_header_length() * 8), _create_u8_vec) >>
    (
        IPv6ExtHeader::new(
            nxt_hdr,
            d1,
            d2,
            opt_data,
        )
    )
));

named!(data_offset_and_flags<&[u8], (u8, u16)>, bits!(pair!(
    take_bits!(u8, 4),
    take_bits!(u16, 12)
)));

named!(parse_tcp_header<&[u8], TcpHeader>, do_parse!(
    src: u16!(Endianness::Big) >>
    dst: u16!(Endianness::Big) >>
    seq_num: u32!(Endianness::Big) >>
    ack_num: u32!(Endianness::Big) >>
    offset_flags: data_offset_and_flags >>
    wnd_size: u16!(Endianness::Big) >>
    chksum: u16!(Endianness::Big) >>
    urg_ptr: u16!(Endianness::Big) >>
    (
        TcpHeader::new(
            src,
            dst,
            seq_num,
            ack_num,
            offset_flags.0,
            offset_flags.1,
            wnd_size,
            chksum,
            urg_ptr
        )
    )
));

named!(parse_udp_hader<&[u8], UdpHeader>, do_parse!(
    src: u16!(Endianness::Big) >>
    dst: u16!(Endianness::Big) >>
    length: u16!(Endianness::Big) >>
    chksum: u16!(Endianness::Big) >>
    (
        UdpHeader::new(
            src,
            dst,
            length,
            chksum
        )
    )
));

named!(parse_icmp_header<&[u8], IcmpHeader>, do_parse!(
    icmp_type: be_u8 >>
    code: be_u8 >>
    chksum: u16!(Endianness::Big) >>
    (
        IcmpHeader::new(
            icmp_type,
            code,
            chksum
        )
    )
));

named!(parse_arp_header<&[u8], ArpHeader>, do_parse!(
    hw_type: u16!(Endianness::Big) >>
    proto_type: u16!(Endianness::Big) >>
    hw_addr_len: be_u8 >>
    proto_addr_len: be_u8 >>
    operation: u16!(Endianness::Big) >>
    sender_hw_address: parse_mac_address >>
    sender_proto_address: pair!(u16!(Endianness::Big), u16!(Endianness::Big)) >>
    target_hw_address: parse_mac_address >>
    target_proto_address: pair!(u16!(Endianness::Big), u16!(Endianness::Big)) >>
    (
        ArpHeader::new(
            hw_type,
            proto_type,
            hw_addr_len,
            proto_addr_len,
            operation,
            sender_hw_address,
            [sender_proto_address.0, sender_proto_address.1],
            target_hw_address,
            [target_proto_address.0, target_proto_address.1]
        )
    )
));

fn _create_u8_vec(data: &[u8]) -> Vec<u8> {
    Vec::from(data)
}

fn _copy_slice(payload: &[u8]) -> Vec<u8> {
    Vec::from(payload)
}

fn _create_mac(address: &[u8]) -> MacAddress {
    MacAddress::new(array_ref![address, 0, 6].clone())
}

fn _create_ipv4(address: &[u8]) -> Ipv4Addr {
    Ipv4Addr::from(array_ref!(address, 0, 4).clone())
}

fn _create_ipv6(address: &[u8]) -> Ipv6Addr {
    Ipv6Addr::from(array_ref!(address, 0, 16).clone())
}

fn _create_2_oct_array(array: &[u8]) -> [u8; 2] {
    array_ref!(array, 0, 2).clone()
}

fn _create_4_oct_array(array: &[u8]) -> [u8; 4] {
    array_ref!(array, 0, 4).clone()
}

fn _copy_checksum(payload: &[u8]) -> [u8; 16] {
    array_ref!(payload, 0, 16).clone()
}
//#[derive(PartialEq)]
//pub enum Ipv6ExtensionHeader {
//    HopByHopOptions {
//        next_hdr: Ipv6NextHeader,
//        options_and_padding1: u16,
//        options_and_padding2: u32,
//        optional_options_and_padding: Vec<u32>
//    },
//    DestinationOptions {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    Routing {
//        next_hdr: Ipv6NextHeader,
//        routing_type: u8,
//        segments_left: u8,
//        type_specific_data: u32,
//        optional_type_specific_data: Vec<u32>
//    },
//    Fragment {
//        next_hdr: Ipv6NextHeader,
//        fragment_offset: u16,
//        reserved: u8,
//        m_flag: bool,
//        identification: u32
//    },
//    AuthenticationHeader {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    EncapsulatingSecurityPayload {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    Mobility {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    HostIdentityProtocol {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    Shim6Protocol {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//    Reserved {
//        next_hdr: Ipv6NextHeader,
//        data1: u16,
//        data2: u32,
//        more_data: Vec<u32>
//    },
//}

pub fn parse(timestamp: u64, frame: &[u8]) -> Res<ParsedPacket> {
    let (l3packet, eth_frame) = match parse_ether_frame(frame) {
        Ok((rest, eth)) => Ok((rest, eth)),
        Err(e) => Err(format!("Parsing of ethernet frame failed due to: {}", e))
    }?;

    let (l4segment, packet) = match eth_frame.get_ether_type() {
        ether_types::IPV4 => parse_ipv4_packet(l3packet),
        ether_types::IPV6 => parse_ipv6_packet(l3packet),
        ether_types::ARP => parse_arp_packet(l3packet),
        _ => Err(format!("Unknown ether_type: `{}`.", eth_frame.get_ether_type()))
    }?;

    match packet {
        L3Packet::Arp(arp_header) => {
            Ok(ParsedPacket::new_l3(
                eth_frame,
                L3Packet::Arp(arp_header),
                timestamp,
            ))
        },
        L3Packet::Ipv6(ipv6_hdr) => {
            let (payload, segment) =  match ipv6_hdr.get_protocol() {
                segment_types::UDP => parse_udp_segment(l4segment),
                segment_types::ICMP => parse_icmp_segment(l4segment),
                segment_types::TCP => parse_tcp_segment(l4segment),
                _ => Err(format!("Unknown segment type: {}", ipv6_hdr.get_protocol() ))
            }?;

            Ok(ParsedPacket::new_l4(
                eth_frame,
                L3Packet::Ipv6(ipv6_hdr),
                segment,
                timestamp
            ))
        },
        L3Packet::Ipv4(ipv4_hdr) => {
            let (payload, segment) =  match ipv4_hdr.get_protocol() {
                segment_types::UDP => parse_udp_segment(l4segment),
                segment_types::ICMP => parse_icmp_segment(l4segment),
                segment_types::TCP => parse_tcp_segment(l4segment),
                _ => Err(format!("Unknown segment type: {}", ipv4_hdr.get_protocol() ))
            }?;

            Ok(ParsedPacket::new_l4(
                eth_frame,
                L3Packet::Ipv4(ipv4_hdr),
                segment,
                timestamp
            ))
        }
    }
}

fn parse_ipv4_packet(packet: &[u8]) -> Res<(&[u8], L3Packet)> {
    match parse_ipv4(packet) {
        Ok((l4segment, pkt)) => {
            Ok((l4segment, L3Packet::Ipv4(pkt)))
        },
        Err(e) => Err(format!("Paring of IPv4 Packet failed, due to: {}", e))
    }
}

fn parse_ipv6_packet(packet: &[u8]) -> Res<(&[u8], L3Packet)> {
    let (pkt_ext_hdrs_and_segme, mut header) = match parse_ipv6_header(packet) {
        Err(e) => Err(format!("Parsing of IPv6 Packet failed, due to: {}", e)),
        Ok((pkt_and_segme, pkt)) => Ok((pkt_and_segme, pkt))
    }?;

    let mut last_next_hdr = header.get_next_header();
    let mut pkt_data = pkt_ext_hdrs_and_segme;

    while ipv6_next_headers::is_ipv6_ext_header(last_next_hdr) {
        let (rst, mut e_header) = match parse_ipv6_ext_hdr(pkt_data) {
            Ok((rest, ext_header)) => Ok((rest, ext_header)),
            Err(e) => Err(format!("Parsing of IPv6 extension header failed, due to: {}", e))
        }?;
        e_header.set_current_hdr_type(last_next_hdr);
        last_next_hdr = e_header.get_next_hdr().get_next_header();
        pkt_data = rst;
        header.push_ext_header(e_header);
    }

    header.set_protocol(last_next_hdr);

    Ok((pkt_data, L3Packet::Ipv6(header)))
}

fn parse_arp_packet(packet: &[u8]) -> Res<(&[u8], L3Packet)> {
    match parse_arp_header(packet) {
        Ok((rest, arp_pkt)) => {
            Ok((rest, L3Packet::Arp(arp_pkt)))
        },
        Err(e) => Err(format!("Parsing of ARP Packet failed, due to: {}", e))
    }
}

pub fn parse_tcp_segment(segment: &[u8]) -> Res<(&[u8], L4Segment)> {
    match parse_tcp_header(segment) {
        Ok((payload, segme)) => Ok((payload, L4Segment::Tcp(segme))),
        Err(e) => Err(format!("Parsing of TCP segment failed, due to: {}", e))
    }
}

pub fn parse_udp_segment(segment: &[u8]) -> Res<(&[u8], L4Segment)> {
    match parse_udp_hader(segment) {
        Ok((payload, segme)) => Ok((payload, L4Segment::Udp(segme))),
        Err(e) => Err(format!("Parsing of UDP segment failed, due to: {}", e))
    }
}

pub fn parse_icmp_segment(segment: &[u8]) -> Res<(&[u8], L4Segment)> {
    match parse_icmp_header(segment) {
        Ok((payload, segme)) => Ok((payload, L4Segment::Icmp(segme))),
        Err(e) => Err(format!("Parsing of ICMP segment failed, due to: {}", e))
    }
}
